let express = require('express');
let bodyParser = require('body-parser');
let db = require('./db/db');
let app = express();
let assert = require('assert');

app.set('views', './views');
app.set('view engine', 'pug');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) {
    res.render('index', { title: 'Hey', header: 'Hi!'});
});
app.get('/about',(req,res)=>{
    res.send('<h1>About us</h1>');
});

app.get('/user/:id',(req,res)=>{
    res.send(`user id is ${req.params.id}`)
});
app.route('/users')
.get(async(req, res)=> {
        let usersList = await db.getUsers();
        res.render('users', { title: 'Users', header: 'Users', userList: usersList});
    })
.post(async function(req, res) {
    console.log(req.body);
    await db.addUser(req.body);
    let usersList = await db.getUsers();
    console.log(usersList);
    res.render('users', { title: 'Users', header: 'Users', userList: usersList});
})
.put(function(req, res) {
        res.send('Update user information');

    });
// app.post('/', function(req, res){
//     console.log(req.body);
//     console.log('post done');
//     res.send('ok');
// });
app.listen(3000);