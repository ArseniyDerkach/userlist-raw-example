const MongoClient = require('mongodb').MongoClient;
const db = require('./config');


exports.connect = () => {
    MongoClient.connect(db.url, { useNewUrlParser: true }, (err, client) => {
        console.log('connected');
        client.close();
    })
};

exports.addUser = (user) => {
    MongoClient.connect(db.url, { useNewUrlParser: true }, (err, client) => {
        let currentdb = client.db(db.dbName);
        currentdb.collection("users").insertOne(user, function(err, res) {
        if (err) throw err;
    });
        client.close()
        })
};

// exports.getUsers = async () => {
//     let users = await MongoClient.connect(db.url, { useNewUrlParser: true }, (err, client) => {
//         let currentdb = client.db(db.dbName);
//         users = currentdb.collection("users").find({}).toArray((function(err, result) {
//             console.log('result');
//             console.log(result);
//             return result;
//         }));
//
//     });
//     console.log(users);
// };




exports.getUsers = async () => {
    let client;

    client = await MongoClient.connect(db.url, { useNewUrlParser: true });


    console.log("Connected correctly to server");

    const currentdb = client.db(db.dbName);

    const col = currentdb.collection('users');

    const users = await col.find({}).toArray();
    client.close;
    return users;
}